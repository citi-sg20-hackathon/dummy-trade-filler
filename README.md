# Dummy Trade Fulfillment Engine

### Outline
This is a simple spring-boot app that will simulate a system that sends trade requests to be executed at an exchange. This system does not actually send the reuests to an exchange, it is just a SIMULATOR for testing.

On a timed interval this service will read from a MongoDB database. It will look for records that represent Trade objects. Each object represents a request to make a trade i.e. a BUY or a SELL.

Each trade request is expected to move through the following states:
1. CREATED : All trade records are initially marked with this state to indicate the request has just been created.
2. PROCESSING : When trade requests have been sent to a simulated exchange they will be marked as PROCESSING to indicate they are currently being handled.
3. FILLED : A trade request that was successfull will be marked as FILLED.
4. REJECTED : A trade requests that was unsuccessfull will be marked as REJECTED.

It will mark all records that it finds with state "CREATED" as "PROCESSING".

It will mark all records that it finds with state "PROCESSING" as either "FILLED" or "REJECTED". The decision between "FILLED" and "REJECTED" is completely random.

You may edit the source code as you require to meet the needs of testing and demonstrating your system.

The two areas you will initially need to update are:
1. The Trade model (com.conygre.training.tradesimulator.model). This should be changed to match your design from the Java Hackathon Trade REST API.
2. You may want to update the application.properties file so that this service uses the same mongodb host and database as your Trade REST API.

### Run
Build on the command line with gradle:

```./gradlew build```

This will put a jar in build/libs/trade-simulator-0.0.1-SNAPSHOT.jar

Run that jar with:

```java -jar build/libs/trade-simulator-0.0.1-SNAPSHOT.jar```

OR for example with DEBUG logging and on a different port (8089):

```java -DLOG_LEVEL=DEBUG -DSERVER_PORT=8089 -jar build/libs/trade-simulator-0.0.1-SNAPSHOT.jar```


OR just run in VSCode or any other IDE for development

### Docker
There is a Dockerfile included to run as a Docker container.

e.g. to build a container:

```docker build -t trade-sim:0.0.1 .```

e.g. to run the built container with DEBUG logging:

```docker run --name trade-sim -e LOG_LEVEL=DEBUG trade-sim:0.0.1```

### Configuration
See the properties file in src/main/resources/application.properties for configurable properties.

Most properties can be overridden by environmental variables.

For Example:

```${DB_HOST:localhost}```
is saying that it'll connect to mongodb at localhost, unless there is an environmental variable called DB_HOST - in which case it'll use the value of that env variable instead

```${DB_NAME:tradedb}```
is saying it'll connect to a database called tradedb unless there is an environmental variable called DB_NAME - in which case it'll use the value of that env variable instead
=======
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

---
### To deploy it:
1. Ensure you have completed the instructions for **Set Up** in the trade-rest-api repository found [here](https://bitbucket.org/citi-sg20-hackathon/trade-rest-api/) before proceeding
2. Run `./gradlew build`
3. Run `sudo docker-compose build`
4. Run `sudo docker-compose up`

### Docker Compose Info
`docker-compose up` creates a network for the containers.
For another container to connect to it, you need to know the network id or network name:

1. The default network name is the name of the repository followed by underscore default.
2. trade-rest-api repository will have a network name of trade-rest-api_default
3. You can see the network id via `docker network ls`

More information on this:

1. [Communication between multiple docker-compose projects](https://stackoverflow.com/questions/38088279/communication-between-multiple-docker-compose-projects)